# -*- coding: utf-8 -*-
"""
Created on Mon May 18 15:09:23 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import numpy as np
import sub.wing_section_methods as wing_section_methods
import sub.naca_maker


class WingSection(object):
    def __init__(self, section_offset, design_properties):

        design_attr_dict = design_properties.__dict__

        self.__dict__.update((key, False) for key in design_attr_dict)

        self.__dict__.update((key, value) for key,
                             value in design_attr_dict.items())

        self.section_offset = section_offset
        self.span = float(self.span_ycoords[-1:] - self.span_ycoords[0])

    def create(self):

        self._sweep_offset = wing_section_methods.sweep_offset(self)
        self._taper_chord = wing_section_methods.taper_chord(self)
        self._xcoord_change = np.add(self._sweep_offset, self._taper_chord)

        self._xcoord_change = np.add(self._xcoord_change, self.section_offset)

        self.section_chord = wing_section_methods.section_chord_length(self)

        self.wing_sections = []
        self._number_of_sections = len(self.span_ycoords)

        if type(self.airfoil) == str:
            self.airfoil = naca_maker.AirfoilMaker(self.airfoil,
                                                   self.airfoil_xcoords)
            self._camber_line = [self.airfoil.x_camber,
                                 self.airfoil.y_camber['yc']]

            self._normalized_airfoil = self.airfoil.surface_coordinates
            self._number_of_points = self.airfoil.n_elements

        else:

            self._normalized_airfoil = self.airfoil
            self._number_of_points = len(self._normalized_airfoil[0])

        for coord in range(self._number_of_sections):
            sized_airfoil = np.multiply(self._normalized_airfoil,
                                        self.section_chord[coord]).tolist()

            shifted_airf_x = np.add([sized_airfoil[0]],
                                    self._xcoord_change[coord]).tolist()

            sized_airf_z = [sized_airfoil[1]]

            self.wing_sections += [shifted_airf_x + 
                                  [[self.span_ycoords[coord]]*self._number_of_points]
                                  + sized_airf_z]

        self.section_offset = float(self._xcoord_change[-1])

    pass

    def __len__(self):
        return self._number_of_sections

    def __getitem__(self, position):
        return self.wing_sections[position]
