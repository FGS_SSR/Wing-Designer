# -*- coding: utf-8 -*-
"""
Created on Fri May  8 16:15:25 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------
"""

from collections import defaultdict
from sub.wing_section_maker import WingSection
import sub.general_methods as general_methods
from sub.sweep_line import sweep_line
from sub.wing_edges import le_te_lines


class WingBuilder:
    def __init__(self, wing_name, wing_design_attr):

        self.airfoil_sections = defaultdict(list)
        self._prev_section_offset = 0
        self.wing_span = 0
        self.wing_design_attr = wing_design_attr

        self.section_chords = defaultdict(list)
        self.chord_offset = defaultdict(list)

        self.wing_name = wing_name
        general_methods.make_folders(wing_name)

    def create(self):

        self.section_order = []
        for wing_section in self.wing_design_attr:

            self.section_order += [wing_section.section_ID]
            self._section_design = WingSection(self._prev_section_offset,
                                               wing_section)

            self._section_design.create()

            self.airfoil_sections[wing_section.section_ID] =\
                self._section_design.wing_sections

            self._prev_section_offset = self._section_design.section_offset

            self.section_chords[wing_section.section_ID] =\
                self._section_design.section_chord

            self.chord_offset[wing_section.section_ID] +=\
                list(self._section_design._xcoord_change)

            self.wing_span += self._section_design.span

        self.sweep_line = sweep_line(self)

        le, te = le_te_lines(self)

        self.leading_edge = le
        self.trailing_edge = te

        self.final_wing_taper = 1
        for section in self.wing_design_attr:
            self.final_wing_taper *= section.taper_ratio

        self.area = general_methods.wing_area(self.wing_design_attr)
        self.aspect_ratio = general_methods.aspect_ratio(self.area, self.wing_span)
