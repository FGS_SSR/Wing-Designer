# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 16:34:39 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Airfoil maker class for 4- and 5-digit airfoils

"""
import sub.naca_methods as naca_methods
from sub.design_constants import airfoil_design_constants


class AirfoilMaker:
    def __init__(self, airfoil, x_coords):

        self.n_elements = 2*int(len(x_coords)) - 1
        self.x_camber = x_coords

        self.airfoil = airfoil

        self.airfoil_param = naca_methods.decode_airfoil(self)

        self.design_constants = airfoil_design_constants()

        # 4 digit NACA airfoil MPXX
        if len(self.airfoil) == 4:
            self.y_camber = naca_methods.camber_coords_mpxx(self)

        # 5 digit NACA airfoil LPQXX
        else:
            self.y_camber = naca_methods.camber_coords_lpqxx(self)

        self.y_thickness = naca_methods.thickness_coordinates(self)

        self.theta = naca_methods.theta_thickness_derivative(self)

        self.surface_coordinates = naca_methods.airfoil_surface(self)
