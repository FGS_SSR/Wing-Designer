# -*- coding: utf-8 -*-
"""
Created on Fri May 29 13:00:26 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import numpy as np
from collections import defaultdict
import sub.general_methods as general_methods


class PlottingData:
    def __init__(self, wing):

        self.plotting_data = defaultdict(dict)
        wing_sections = wing.airfoil_sections

        for section in wing_sections:
            self.plotting_data['wing'][section] =\
                [airfoil for airfoil in wing_sections[section]]

        self.plotting_data['sweep_line'] = wing.sweep_line

        self.plotting_data['leading_edge'] = wing.leading_edge

        self.plotting_data['trailing_edge'] = wing.trailing_edge

        self.plotting_data['wing_area'] = wing.area
        self.plotting_data['aspect_ratio'] = wing.aspect_ratio
        self.plotting_data['taper_ratio'] = wing.final_wing_taper
        self.plotting_data['wing_name'] = wing.wing_name

        # define plotting ranges
        # organize leading and trailing edge coords to determine bounds more easily

        le_coords_x = [section[0] for section in wing.leading_edge]
        le_coords_y = [section[1] for section in wing.leading_edge]
        te_coords_x = [section[0] for section in wing.trailing_edge]

        # x range
        # x_min = np.min(le_coords_x)
        x_min = 0
        x_max = np.max(te_coords_x)

        self.x_range = [general_methods.multiplier_round(1.25*x_min, 5),
                        general_methods.multiplier_round(1.25*x_max, 5)]
        # y range
        y_min = np.min(le_coords_y)
        y_max = np.max(le_coords_y)

        self.y_range = [general_methods.multiplier_round(1.25*y_min, 5),
                   general_methods.multiplier_round(1.25*y_max, 5)]

        # z range (defined as a percentage of the y_range)
        z_coords = []
        for section in wing_sections:
            for airfoil in wing_sections[section]:
                z_coords += airfoil[2]

        self.z_range = [general_methods.multiplier_round(-y_max*0.15, 5),
                   general_methods.multiplier_round(y_max*0.15, 5)]
