# -*- coding: utf-8 -*-
"""
Created on Fri May 29 23:03:48 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: this function extracts the quarter chord sweep line for each wing section

"""
from collections import defaultdict


def sweep_line(self):

    section_airfoils = self.airfoil_sections
    section_chords = self.section_chords
    chord_offset = self.chord_offset

    sweep_line = []

    z_coords = [0, 0]
    for section in section_airfoils:
        y_coords = [section_airfoils[section][0][1][0],
                    section_airfoils[section][-1][1][0]]

        quarter_chord_loc = [chord_offset[section][0]
                             + 0.25*section_chords[section][0],
                             chord_offset[section][-1]
                             + 0.25*section_chords[section][-1]]

        sweep_line += [[quarter_chord_loc, y_coords, z_coords]]

    return sweep_line
