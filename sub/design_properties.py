# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 11:42:31 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
from collections import defaultdict
import numpy as np


class WingSectionProperties(object):
    def __init__(self, wing_section_design):

        design_attributes = ['airfoil', 'airfoil_ID', 'sweep_angle',
                             'root_chord', 'taper_ratio', 'span_ycoords',
                             'airfoil_xcoords', 'section_ID']

        rejected_keys = [k for k in set(wing_section_design.keys()) if k not in
                         design_attributes]

        if rejected_keys:
            raise ValueError("Invalid arguments in design properties:{}."
                             .format(rejected_keys))

        self.__dict__.update((key, False) for key in design_attributes)

        self.__dict__.update((key, value) for key,
                             value in wing_section_design.items())


def fill_section_properties(sections):

    default_attr_list = ['taper_ratio', 'airfoil', 'airfoil_ID',
                         'airfoil_xcoords', 'sweep_angle', 'root_chord']

    default_attr = defaultdict(list)
    default_attr.update((key, False) for key in default_attr_list)

    for section in sections:
        section.update((key, value) for key, value in default_attr.items()
                       if key not in section.keys() and key != 'root_chord')

        if not section['root_chord'] or section['root_chord'] == 'match':
            section['root_chord'] = default_attr['root_chord']\
                                    * default_attr['taper_ratio']

        default_attr.update((key, value) for key,
                            value in section.items()
                            if key in default_attr_list)

    return sections
#
def validate_input(section):

    attr_list = list(section.__dict__.keys())

    for attribute in attr_list:
        val = section.__getattribute__(attribute)

        if attribute == 'airfoil':

            if type(val)== str:

                assert int(val),\
                    'Airfoil string should only contain digits.'

                assert len(val) in {4, 5},\
                    'Airfoil identifier not a 4- or 5-digit NACA airfoil'

                if len(val) == 5:
                    assert int(val[2]) == 0,\
                    'This code does not currently handle reflex airfoils. Third airfoil digit must be 0.'

                    assert int(val[1]) in {1, 2, 3, 4, 5}, \
                        'Maximum camber position (second digit) outside possible range for a NACA 5 digit airfoil.'

            elif type(val) in {list, 'numpy.ndarray'}:
                assert len(val) == 2,\
                    "Airfoil coordinates should have the following format: [[x_coords], [y_coords]]."
                for entry in val:
                    assert len(entry) > 2,\
                        'not enough coordinates to create an airfoil.'
            else:

                raise TypeError('Airfoil format is incorrect. See the documentation for input format.')

        if attribute == 'airfoil_ID':
            assert type(val) == str,\
                "{} must be a string.".format(attribute)

            assert len(val) < 64,\
                "{} exceeds the maximum number of 64 characters.".format(attribute)

        if attribute == 'sweep_angle':
            assert type(val) == int or type(val) == float,\
                '{} must be an integer or float.'.format(attribute)

            assert -90 < val < 90,\
                'sweep_angle is outside the permissible range of -90 < angle < 90 degrees.'

        if attribute == 'root_chord':
            assert type(val) == int or type(val) == float,\
                '{} must be an integer or float.'.format(attribute)

            assert val > 0,\
                '{} must be given as a positive int or float.'.format(attribute)

        if attribute == 'taper_ratio':
            assert type(val) == int or type(val) == float,\
                '{} must be an integer or float.'.format(attribute)

            assert val > 0,\
                '{} must be given as a positive int or float.'.format(attribute)

        if attribute == 'span_ycoords':
            assert type(val) in {list, np.ndarray},\
                '{} must be given as a 1D list or numpy array containing\
longitudinal coordinates of all wing sections'.format(attribute)

            assert len(val) > 1,\
                '{} must contain at least two coordinates'.format(attribute)

        if attribute == 'airfoil_xcoords':
            assert type(val) in {list, np.ndarray},\
                '{} must be given as a 1D list or numpy array containing\
chord coordinates of the airfoil'.format(attribute)

            assert len(val) > 1,\
                '{} must contain at least two coordinates.'.format(attribute)

        if attribute == 'section_ID':
            assert type(val) == str,\
                "{} must be a string.".format(attribute)

            assert len(val) < 64,\
                "{} exceeds the maximum number of 64 characters.".format(attribute)
