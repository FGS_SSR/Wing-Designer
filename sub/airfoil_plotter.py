# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 22:03:05 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""

import matplotlib.pyplot as plt

def airfoil_plotter(data_set, chord, thk_range):

    plt.figure(figsize=(10,10))
    plt.xlim([0, chord])
    plt.ylim(thk_range)

    for data in data_set:
        plt.plot(data[0], data[1])

    plt.show()