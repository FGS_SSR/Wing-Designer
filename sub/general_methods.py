# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 12:05:10 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: file for general methods that are not specific to the wing maker

"""
import numpy as np
import os


def multiplier_round(x, base):

    if x > 0:
        rounding = base * int(np.ceil(x/base))
    else:
        rounding = base * int(np.floor(x/base))

    return rounding


def wing_area(wing_design_properties):

    section_area = []

    for section in wing_design_properties:

        root_c = section.root_chord

        taper_ratio = section.taper_ratio

        span = max(section.span_ycoords)\
            - min(section.span_ycoords)

        # trapezoidal wing area
        section_area += [0.5 * (root_c * (1 + taper_ratio)) * span]

    return sum(section_area)


def cosine_coordinates(n_elements):

    pi_val = np.pi
    pi_range = np.linspace(0, pi_val, int(n_elements/2))

    x_coords = []

    for val in pi_range:
        x_coords += [(1+np.cos(val))/2]

    return x_coords


def aspect_ratio(wing_area, wing_span):

    aspect_ratio = pow(wing_span, 2)/wing_area

    return aspect_ratio


def make_folders(wing_name):

    os.makedirs('output_data', exist_ok=True)

    # os.makedirs(wing_name, exist_ok=True)

    wing_dir = os.path.join('output_data', '{}'.format(wing_name))

    os.makedirs(wing_dir, exist_ok=True)

    os.chdir(wing_dir)

    os.makedirs('plots', exist_ok=True)
    os.makedirs('coordinates', exist_ok=True)
    os.makedirs('CAD_export', exist_ok=True)

    os.chdir('../../')

    pass
