# -*- coding: utf-8 -*-
"""
Created on Thu May  7 16:57:05 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: design constants of the naca airfoils

"""
from collections import defaultdict


def airfoil_design_constants():

    # surface plotting coefficients
    a_coeff = [0.2969, -0.1260, -0.3516, 0.2843, -0.1015]

    # 5 digit airfoil coefficients
    r_coeff = {1: 0.058, 2: 0.126, 3: 0.2025, 4: 0.29, 5: 0.391}
    k1_coeff = {1: 361.4, 2: 51.64, 3: 15.957, 4: 6.643, 5: 3.23}

    design_constants = defaultdict(dict)
    design_constants['a_coeff'] = a_coeff
    design_constants['r_coeff'] = r_coeff
    design_constants['k1_coeff'] = k1_coeff

    return design_constants
