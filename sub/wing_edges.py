# -*- coding: utf-8 -*-
"""
Created on Sun May 31 23:48:26 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: function retrieves leading and trailing edge
coordinates from wing sections

"""


def le_te_lines(self):

    le_x = []
    le_y = []
    le_z = []
    te_x = []
    te_y = []
    te_z = []

    wing_sections = self.airfoil_sections

    leading_edge = []
    trailing_edge = []

    for section in wing_sections:
        root_airfoil = wing_sections[section][0]
        tip_airfoil = wing_sections[section][-1]

        idx_te_root = root_airfoil[0].index(max(root_airfoil[0]))
        idx_te_tip = tip_airfoil[0].index(max(tip_airfoil[0]))

        te_x = [root_airfoil[0][idx_te_root], tip_airfoil[0][idx_te_tip]]
        te_y = [root_airfoil[1][idx_te_root], tip_airfoil[1][idx_te_tip]]
        te_z = [root_airfoil[2][idx_te_root], tip_airfoil[2][idx_te_tip]]

        idx_le_root = root_airfoil[0].index(min(root_airfoil[0]))
        idx_le_tip = tip_airfoil[0].index(min(tip_airfoil[0]))

        le_x = [root_airfoil[0][idx_le_root], tip_airfoil[0][idx_le_tip]]
        le_y = [root_airfoil[1][idx_le_root], tip_airfoil[1][idx_le_tip]]
        le_z = [root_airfoil[2][idx_le_root], tip_airfoil[2][idx_le_tip]]

        leading_edge += [[le_x, le_y, le_z]]
        trailing_edge += [[te_x, te_y, te_z]]

    return leading_edge, trailing_edge
