# -*- coding: utf-8 -*-
"""
Created on Tue May 12 10:10:33 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
from naca_maker import AirfoilMaker
from csv_reader import csv_reader
from collections import defaultdict

from general_methods import cosine_coordinates
from airfoil_plotter import airfoil_plotter

import numpy as np


class WingMakerTests:
    def __init__(self):
        pass
    def airfoil_coord_test(self):

        validation_data = {}
        test_airfoils = ['2412', '24012']

        for airfoil in test_airfoils:
            validation_data[airfoil] = csv_reader('data/naca{}_validation.csv'.format(airfoil),
                                                  ['x_coords', 'func_val'])

        # verification data #
        airfoil_verification_data = {}
        x_coordinates = cosine_coordinates(81)

        for airfoil in test_airfoils:
            generated_airfoil = AirfoilMaker(airfoil,
                                             x_coordinates)

            airfoil_verification_data[airfoil] = generated_airfoil.surface_coordinates

        return airfoil_verification_data, validation_data

verification = WingMakerTests()

a, b = verification.airfoil_coord_test()

data = [[a['24012'][0], a['24012'][1]], [b['24012']['x_coords'], b['24012']['func_val']]]

airfoil_plotter(data, 1, [-0.25, 0.25])
airfoil_plotter(data, 0.02, [-0.05, 0.05])