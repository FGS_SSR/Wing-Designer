# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 12:06:40 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: NACA 4 methods

"""
import numpy as np


def decode_airfoil(self):

    airfoil = self.airfoil

    if len(airfoil)==4:
        M = float(airfoil[0])/100
        P = float(airfoil[1])/10
        XX = float(airfoil[2:])/100

        airfoil_param = {'M': M, 'P': P, 'XX': XX}

    else:
        L = (3/2)*float(airfoil[0])/10
        P = float(airfoil[1])/20
        Q = int(airfoil[2])
        XX = float(airfoil[3:])/100

        airfoil_param = {'L': L, 'P': P, 'Q': Q, 'XX': XX}

    return airfoil_param


def camber_coords_mpxx(self):
    airfoil_param = self.airfoil_param
    x_camber = self.x_camber

    yc = []
    dyc = []
    M = airfoil_param['M']
    P = airfoil_param['P']

    for x in x_camber:
        if x < P:
            yc += [M*x / pow(P, 2) * (2*P - x)]
            dyc += [2*M / pow(P, 2) * (P - x)]

        else:
            yc += [(M*(1-x) / pow(1 - P, 2)) * (1 + x - 2*P)]
            dyc += [(2*M / pow(1 - P, 2)) * (P - x)]

    y_camber = {'yc': yc, 'dyc': dyc}

    return y_camber

def camber_coords_lpqxx(self):
    airfoil_param = self.airfoil_param
    x_camber = self.x_camber

    yc = []
    dyc = []
    L = airfoil_param['L']
    P = airfoil_param['P']
    Q = airfoil_param['Q']

    # design constants for the selected thickest part 
    k1_coeff = self.design_constants['k1_coeff'][int(P*20)]
    r_coeff = self.design_constants['r_coeff'][int(P*20)]

    k1 = L * k1_coeff/0.3
    r = r_coeff

    for x in x_camber:
        if x < r:
            yc += [(1/6)*k1*(pow(x, 3)
                            - 3*r*pow(x, 2) + pow(r, 2)*(3-r)*(x))]

            dyc += [(1/6)*k1*(3*pow(x, 2)
                             - 6*r*x + pow(r, 2)*(3-r))]

        else:
            yc += [(1/6)*k1*pow(r, 3)*(1-x)]

            dyc += [-k1*pow(r, 3)/6]

    y_camber = {'yc': yc, 'dyc': dyc}

    return y_camber


def thickness_coordinates(self):
    airfoil_param = self.airfoil_param
    x_camber = self.x_camber
    a_coeff = self.design_constants['a_coeff']

    yt = []
    a0 = a_coeff[0]
    a1 = a_coeff[1]
    a2 = a_coeff[2]
    a3 = a_coeff[3]
    a4 = a_coeff[4]

    XX = airfoil_param['XX']

    for x in x_camber:
        yt += [5*XX * (a0 * np.sqrt(x) + a1 * x + a2 * pow(x, 2) +
                     a3 * pow(x, 3) + a4 * pow(x, 4))]

    y_thickness = yt

    return y_thickness


def theta_thickness_derivative(self):

    dyc = self.y_camber['dyc']
    theta = []

    for slope in dyc:
        theta += [np.arctan(slope)]

    return theta


def airfoil_surface(self):

    x_camber = self.x_camber
    y_camber = self.y_camber['yc']
    y_thickness = self.y_thickness
    theta = self.theta

    coord_number = len(x_camber)
    # reverse coordinate lists for prettier plotting (lower surface coords)
    x_camber_low = list(reversed(x_camber[:-1]))
    y_camber_low = list(reversed(y_camber[:-1]))
    y_thickness_low = list(reversed(y_thickness[:-1]))
    theta_low = list(reversed(theta[:-1]))

    xu_surf = []
    xl_surf = []
    yu_surf = []
    yl_surf = []

    for val in range(coord_number):
        # Upper surface coordinates
        xu_surf += [(x_camber[val] -
                     y_thickness[val]*np.sin(theta[val]))]
        yu_surf += [(y_camber[val] +
                     y_thickness[val]*np.cos(theta[val]))]

    for val in range(coord_number-1):

        # Lower surface coordinates
        xl_surf += [(x_camber_low[val]
                    + y_thickness_low[val]*np.sin(theta_low[val]))]
        yl_surf += [(y_camber_low[val] -
                     y_thickness_low[val]*np.cos(theta_low[val]))]

    airfoil_coords = [xu_surf + xl_surf, yu_surf + yl_surf]

    return airfoil_coords
