# -*- coding: utf-8 -*-
"""
Created on Mon May 18 16:45:11 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import numpy as np


def sweep_offset(self):
    sweep_radians = np.deg2rad(self.sweep_angle)
    slope = np.tan(sweep_radians)
    sweep_offset = []
    b = -self.span_ycoords[0]*slope #y = mx + b

    for x in self.span_ycoords:
        sweep_offset += [x*slope + b]

    return sweep_offset


def taper_chord(self):
    tip_chord = self.root_chord * self.taper_ratio
    slope = 0.25*(self.root_chord-tip_chord) / self.span
    taper_chord = []
    b = -self.span_ycoords[0]*slope #y = mx + b

    for x in self.span_ycoords:
        taper_chord += [x*slope + b]

    return taper_chord


def section_chord_length(self):
    tip_chord = self.root_chord * self.taper_ratio
    slope = (-self.root_chord+tip_chord) / self.span

    b = float(self.root_chord - slope*self.span_ycoords[0]) #y = mx + b

    section_chord = []

    for x in self.span_ycoords:
        section_chord += [slope*x + b]

    return section_chord
