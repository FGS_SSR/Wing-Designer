# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 17:08:28 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
from sub.csv_reader import csv_reader


def get_airfoil_coordinates(file):

    data_headers = ['x', 'y']
    coordinates = csv_reader(file, data_headers)

    airfoil = [coordinates['x'], coordinates['y']]

    return airfoil
