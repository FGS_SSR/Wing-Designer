# -*- coding: utf-8 -*-
"""
Created on Sun May 24 23:43:27 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: 3D plotting for all wing sections

"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
import os


class WingPlot:
    def __init__(self, plot_settings, plot_data):
        self.fig = plt.figure(figsize=(10, 10))
        self.ax = plt.axes(projection='3d')

        self.plot_data = plot_data
        self.plot_settings = plot_settings

    def create(self):

        data = self.plot_data.plotting_data
        ax = self.ax
        plot_data = self.plot_data
        plot_settings = self.plot_settings

        ax.set_xlim(plot_data.x_range)
        ax.set_ylim(plot_data.y_range)
        ax.set_zlim3d(plot_data.z_range)

        ax.set_title(plot_settings['title'], fontsize=16, loc='left')

        ax.set_xlabel('x [-]')
        ax.set_ylabel('y [-]')
        ax.set_zlabel('z [-]')
        colour_list = ['black', '#e50000', '#029386',
                       '#276ab3', '#d5ab09', '#f97306']

        colour_counter = 0
        section_counter = 0
        legend_patches = []
        # wing cx plotting
        for section in data['wing']:
            airfoils = data['wing'][section]

            for index in range(section_counter, len(airfoils)):

                af_x = airfoils[index][0]
                af_y = airfoils[index][1]
                af_z = airfoils[index][2]
                ax.plot3D(af_x, af_y, af_z, label=section, c=colour_list[colour_counter])

            if plot_settings['section_color'] is True:
                legend_patches += [mpatches.Patch(color=colour_list[colour_counter], label=section)]

                plt.legend(bbox_to_anchor=(0.12,0.8), loc="lower left", 
                           bbox_transform=self.fig.transFigure, handles=legend_patches)

                colour_counter += 1

            section_counter = 1

        text = '\n'.join((
            r'Wing Area (S)$=%.2f$' % (data['wing_area']),
            r'Aspect Ratio$=%.2f$' % (data['aspect_ratio']),
            r'Taper Ratio ($\lambda$)$=%.2f$' % (data['taper_ratio'])))

        ax.text2D(0.01, 0.05, text, transform=ax.transAxes, fontsize=12)
        # sweep line plotting
        colour_counter = 0

        if plot_settings['sweep'] is True:
            for section in data['sweep_line']:
                sweep_x = section[0]
                sweep_y = section[1]
                sweep_z = section[2]

                ax.plot3D(sweep_x, sweep_y, sweep_z, c=colour_list[colour_counter])

                if plot_settings['section_color'] is True:
                    colour_counter += 1

        # leading and trailing edge plotting
        if plot_settings['edges'] is True:
            colour_counter = 0
            for section in data['leading_edge']:
                le_x = section[0]
                le_y = section[1]
                le_z = section[2]

                ax.plot3D(le_x, le_y, le_z, c=colour_list[colour_counter])

                if plot_settings['section_color'] is True:
                    colour_counter += 1

            colour_counter = 0
            for section in data['trailing_edge']:
                te_x = section[0]
                te_y = section[1]
                te_z = section[2]

                ax.plot3D(te_x, te_y, te_z, c=colour_list[colour_counter])

                if plot_settings['section_color'] is True:
                    colour_counter += 1

        ax.view_init(plot_settings['plot_rotation'][0], plot_settings['plot_rotation'][1])

        pass

    def save_plot(self, file_name, file_format):

        data = self.plot_data.plotting_data

        rotation_y = self.plot_settings['plot_rotation'][0]
        rotation_z = self.plot_settings['plot_rotation'][1]

        file_name +='_{}y_rot_{}z_rot'.format(rotation_y, rotation_z)
        plot_folder = os.path.join('output_data/{}'.format(data['wing_name']), 'plots')
        save_loc = os.path.join(plot_folder, '{}.{}'.format(file_name, file_format))

        plt.savefig(save_loc)

        pass
