# -*- coding: utf-8 -*-
"""
Created on Fri May  8 16:48:31 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Use this to generate various tables to compare aspect ratio, wing surface and span

"""
import numpy as np


class DesignTableCreator:
    def __init__(self):
        pass

    def ar_span_tables(self, aspect_ratio_range, span_range):

        ar_entries = len(aspect_ratio_range)

        span_entries = len(span_range)

        comparison_table = np.zeros((ar_entries+1, span_entries+1))

        comparison_table[0][0] = 9999
        comparison_table[0, 1:] = span_range
        comparison_table[1:, 0] = aspect_ratio_range

        for row in range(ar_entries):
            for col in range(span_entries):
                surface_area = pow(span_range[col], 2)/aspect_ratio_range[row]
                comparison_table[row+1, col+1] = surface_area

        return comparison_table

    def area_span_tables(self, area_range, span_range):

        area_entries = len(area_range)

        span_entries = len(span_range)

        comparison_table = np.zeros((area_entries+1, span_entries+1))

        comparison_table[0][0] = 9999
        comparison_table[0, 1:] = span_range
        comparison_table[1:, 0] = area_range

        for row in range(area_entries):
            for col in range(span_entries):
                aspect_ratio = pow(span_range[col], 2)/area_range[row]
                comparison_table[row+1, col+1] = aspect_ratio

        return comparison_table

    def area_ar_tables(self, area_range, aspect_ratio_range):

        ar_entries = len(aspect_ratio_range)

        area_entries = len(area_range)

        comparison_table = np.zeros((ar_entries+1, area_entries+1))

        comparison_table[0][0] = 9999
        comparison_table[0, 1:] = area_range
        comparison_table[1:, 0] = aspect_ratio_range

        for row in range(ar_entries):
            for col in range(area_entries):
                span = np.sqrt(area_range[col]*aspect_ratio_range[row])
                comparison_table[row+1, col+1] = span

        return comparison_table
