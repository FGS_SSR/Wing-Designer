# -*- coding: utf-8 -*-
"""
Created on Tue May 12 10:33:48 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""

import csv
from collections import defaultdict


def csv_reader(file_loc, data_headers):

    data_columns = defaultdict(list)
    header_count = len(data_headers)

    with open(file_loc) as datafile:

        readCSV = csv.reader(datafile, delimiter=',')

        for row in readCSV:
            for header in range(header_count):
                data_columns[data_headers[header]] += [float(row[header])]

    return data_columns