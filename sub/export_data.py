# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 17:07:12 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
import csv
import os
import numpy as np
import warnings


class DataExport:
    def __init__(self, settings, wing):
        self.settings = settings
        self.raw_wing_data = wing

        self.formated_data = DataExport.format_data(settings, wing)

    def format_sweep_edges(list_data):

        formated_coords = [[], [], []]
        counter = 0

        for coord_set in list_data:
            for N in range(3):

                if counter == 1:
                    coord_val = coord_set[N][1:]
                else:
                    coord_val = coord_set[N]

                formated_coords[N] += coord_val
            counter = 1

        return formated_coords

    def format_data(settings, wing):

        export_data = []

        for section in wing.section_order:
            airfoil_list = wing.airfoil_sections[section]

            for airfoil in airfoil_list:
                export_data += [airfoil]

        if settings.export_sweep_line is True:
            sweep_data = DataExport.format_sweep_edges(
                            wing.sweep_line)
            export_data += [sweep_data]

        if settings.export_le_te is True:
            le_data = DataExport.format_sweep_edges(
                            wing.leading_edge)
            te_data = DataExport.format_sweep_edges(
                            wing.trailing_edge)

            export_data += [le_data]
            export_data += [te_data]

        return export_data

    # converts a list of N x,y,z coordinates into a Nx3 array
    def coordinate_array(coordinate_set):

        n_coords = len(coordinate_set[0])
        coord_array = np.zeros((n_coords, 3))

        for idx in range(3):
            coord_array[:, idx] = coordinate_set[idx]

        return coord_array

    def catia_export(self, file, export_data):

        if os.path.isfile(file) is True:
            warnings.warn("""Coordinate file already exists. 
                          Consider saving coordinates to a different file
                          if you don't intend to append multiple designs.""")

        else:
            with open(file, 'w', newline='') as outfile:
                export_file = csv.writer(outfile)
                export_file.writerow(['StartLoft'])
                outfile.close()

        N_coords = len(export_data)
        counter = 0

        for coord_set in export_data:
            point_list = DataExport.coordinate_array(coord_set)

            with open(file, 'a', newline='') as coord_file:

                export_file = csv.writer(coord_file)
                export_file.writerow(['StartCurve'])
                export_file.writerows(point_list)
                export_file.writerow(['EndCurve'])

                counter += 1

                if counter == N_coords:
                    export_file.writerow(['EndLoft'])
                    export_file.writerow(['End'])

            coord_file.close()

    pass


class settings:
    export_sweep_line = True
    export_le_te = True
    file = 'test_catia_export.csv'

data = DataExport(settings, wing)

data.catia_export(settings.file, data.formated_data)