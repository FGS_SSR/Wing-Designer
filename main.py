# -*- coding: utf-8 -*-
"""
Created on Sat Mar 28 13:45:07 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description:

"""
from input_data import input_data
import sub.PlottingData as PlottingData
from sub.wing3Dplot import WingPlot
from sub.wing_builder import WingBuilder
from collections import defaultdict

if __name__ == "__main__":

    des_attr = input_data()
    wing_name = 'test wing'

    wing = WingBuilder(wing_name, des_attr)

    wing.create()

    plotting_data = PlottingData.PlottingData(wing)

    plot_settings = defaultdict(list)

    plot_settings['title'] = 'Test wing plot'
    plot_settings['sweep'] = True
    plot_settings['edges'] = True
    plot_settings['section_color'] = True
    plot_settings['file_name'] = 'test_file'
    plot_settings['plot_rotation'] = [45, 45] # [y-axis rotation, z-axis rotation ] in degrees

    wing_plot = WingPlot(plot_settings, plotting_data)
    wing_plot.create()
    wing_plot.save_plot('test_plot_2sections', 'png')
