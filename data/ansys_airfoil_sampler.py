# -*- coding: utf-8 -*-
"""
Created on Tue May 18 12:20:57 2021

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: This method is used to sample the airfoil coordinates retrieved
from Ansys and reduces the number of coordinates to accomodate a 3 decimal precision

"""

import csv
from collections import defaultdict
import numpy as np


def read_airfoil_coordinates(file_loc):

    data_headers = ['x-coordinate', 'y-coordinate']
    data_columns = defaultdict(list)
    header_count = len(data_headers)

    with open(file_loc) as datafile:

        readCSV = csv.reader(datafile, delimiter=',')

        for row in readCSV:
            for header in range(header_count):
                data_columns[data_headers[header]] += [float(row[header])]

    return data_columns

airfoil_coordinates = read_airfoil_coordinates('pre_filtered_airfoil_FV1.csv')

# this method filters repeating coordinates to reduce number of points

# normalize airfoil dimensions to unit value
def normalize_airfoil_coords(sampled_coordinates):

    max_coord = max(sampled_coordinates['x-coordinate'])

    for coordinate in sampled_coordinates:
        sampled_coordinates[coordinate] = np.divide(sampled_coordinates[coordinate], max_coord)

    return sampled_coordinates

normalized_airfoil_coordinates = normalize_airfoil_coords(airfoil_coordinates)


def airfoil_coordinate_sampler(airfoil_coordinates):
    sampled_airfoil_coordinate = defaultdict(list)
    sampled_airfoil_coordinate['x-coordinate'] = []
    sampled_airfoil_coordinate['x-coordinate'] = []
    for index in range(len(airfoil_coordinates['x-coordinate'])):

        if index == 0:
            sampled_airfoil_coordinate['x-coordinate'] += \
                [round(airfoil_coordinates['x-coordinate'][index], 3)]

            sampled_airfoil_coordinate['y-coordinate'] += \
                [round(airfoil_coordinates['y-coordinate'][index], 3)]

        else:
            reference_coordinate = \
                round(sampled_airfoil_coordinate['x-coordinate'][-1], 3)

            if round(airfoil_coordinates['x-coordinate'][index], 3) == reference_coordinate:
                continue

            else:
                sampled_airfoil_coordinate['x-coordinate'] += \
                    [round(airfoil_coordinates['x-coordinate'][index], 3)]

                sampled_airfoil_coordinate['y-coordinate'] += \
                    [round(airfoil_coordinates['y-coordinate'][index], 3)]

    return sampled_airfoil_coordinate

sampled_coordinates = airfoil_coordinate_sampler(airfoil_coordinates)

# Print airfoil to csv file

def export_to_csv(filename, export_data):

    write_data = []
    #convert coordinates to a 2xN matrix
    for index in range(len(export_data['x-coordinate'])):
        write_data += [[export_data['x-coordinate'][index], export_data['y-coordinate'][index]]]

    with open(filename, 'w', newline='') as outfile:
        export_file = csv.writer(outfile)
        export_file.writerows(write_data)
        outfile.close()

export_to_csv('test_ansys_foil.csv', sampled_coordinates)