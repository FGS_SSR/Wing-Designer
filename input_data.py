# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 11:42:31 2020

@author: Frederico Vicente   e-mail: frederico.gsv@gmail.com
------------------------------------------------------------

File description: Here you can define the wing or shape as a function of cross-section(airfoil), dimensions
sweep angle or taper ratio. See examples below for a 3-section wing. If certain properties are not define for some sections,
these are inherited from the section that precedes them. In the example below, 
the sweep angle was defined once for the root section (5 degrees), 
and the mid and tip sections inherited this value.

"""
import numpy as np
import sub.general_methods as general_methods
import sub.design_properties as design_properties
from sub.get_airfoil_coords import get_airfoil_coordinates
from collections import defaultdict


def input_data():
    ### Edit names or add more sections here, update "wing_settings" list accordingly
    root_section = defaultdict(list)
    tip_section = defaultdict(list)
    mid_section = defaultdict(list)

    root_section['airfoil'] = get_airfoil_coordinates('data/test_ansys_foil.csv')
    root_section['airfoil_ID'] = 'FV1'
    root_section['airfoil_xcoords'] = general_methods.cosine_coordinates(50) # number of points, not used for imported coordinates

    root_section['section_ID'] = 'root'
    root_section['sweep_angle'] = 5 # Degrees
    root_section['root_chord'] = 12 # Length
    root_section['taper_ratio'] = 0.6 # Ratio, dimensionless
    root_section['span_ycoords'] = list(np.linspace(0, 40, 6)) # Defines start and end of section, number of cross sections

    mid_section['section_ID'] = 'tip'
    mid_section['taper_ratio'] = 0.8
    mid_section['span_ycoords'] = np.linspace(40, 140, 6)

#    tip_section['section_ID'] = 'tip'
#    tip_section['taper_ratio'] = 0.6
#    tip_section['span_ycoords'] = np.linspace(80, 140, 8)

    wing_settings = [root_section, mid_section]

    ### No editing beyond this line.
    
    wing_settings = design_properties.fill_section_properties(wing_settings)

    wing_design_attr = []

    for section in wing_settings:
        section_attr = design_properties.WingSectionProperties(section)

        design_properties.validate_input(section_attr)

        wing_design_attr += [section_attr]

    return wing_design_attr
